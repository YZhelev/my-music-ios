//
//  MMSong.swift
//  MyMusic
//
//  Created by MacBook Pro on 7.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation
import FMDB.FMResultSet

class MMSong {
    
    private(set) var id         : Int
    private(set) var playListId : Int
    private(set) var name       : String
    
    init(set: FMResultSet) {
        self.id         = Int(set.int(forColumn: "id"))
        self.playListId = Int(set.int(forColumn: "playlist_id"))
        self.name       = set.string(forColumn: "name") ?? ""
    }
    
    init(name: String, playlistId: Int) {
        self.id         = -1
        self.playListId = playlistId
        self.name       = name
    }
    
}

extension MMSong {
    
    // MARK: Database Helpers
    
    static let createTableQuery = """
    CREATE TABLE IF NOT EXISTS \(MMDatabaseTables.songs)
    (
    \(MMDatabaseFields.id) INTEGER PRIMARY KEY AUTOINCREMENT,
    \(MMDatabaseFields.playlistId) INTEGER NOT NULL,
    \(MMDatabaseFields.name) TEXT NOT NULL
    );
    """
    
    var insertQuery: String {
        return """
        INSERT INTO \(MMDatabaseTables.songs)
        (
        \(MMDatabaseFields.playlistId),
        \(MMDatabaseFields.name)
        )
        VALUES (?, ?);
        """
    }
    
    var values: [Any] {
        return [
            self.playListId,
            self.name
        ]
    }
}
