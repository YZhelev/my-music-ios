//
//  MMPlaylist.swift
//  MyMusic
//
//  Created by MacBook Pro on 7.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation
import FMDB.FMResultSet

class MMPlaylist {
    
    private(set) var id   : Int
    private(set) var name : String
    
    init(set: FMResultSet) {
        self.id   = Int(set.int(forColumn: "id"))
        self.name = set.string(forColumn: "name") ?? ""
    }
    
    init(name: String) {
        self.id   = -1
        self.name = name
    }
    
}

extension MMPlaylist {
    
    // MARK: Database Helpers
    
    static let createTableQuery = """
    CREATE TABLE IF NOT EXISTS \(MMDatabaseTables.playlists)
    (
    \(MMDatabaseFields.id) INTEGER PRIMARY KEY AUTOINCREMENT,
    \(MMDatabaseFields.name) TEXT NOT NULL
    );
    """
    
    var insertQuery: String {
        return """
        INSERT INTO \(MMDatabaseTables.playlists)
        (
        \(MMDatabaseFields.name)
        )
        VALUES (?);
        """
    }
    
    var values: [Any] {
        return [
            self.name
        ]
    }
    
}
