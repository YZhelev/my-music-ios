//
//  UIAlertController+Helpers.swift
//  MyMusic
//
//  Created by MacBook Pro on 6.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    // MARK: Typealiases
    
    typealias MMHandlerBlock = () -> Void
    
    
    // MARK: Public Methods
    
    class func showMessage(_ message: String, sender: UIViewController, dismissAction: MMHandlerBlock? = nil) {
        let okAction = self.okAction(sender, handler: dismissAction)
        
        self.showAlert(MMStrings.myMusic, message: message, actions: [okAction], sender: sender)
    }
    
    
    // MARK: Private Methods
    
    private class func showAlert(_ title: String?, message: String?, actions: [UIAlertAction], preferredAction: UIAlertAction? = nil, sender: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions {
            alertController.addAction(action)
        }
        
        alertController.preferredAction = preferredAction
        
        sender.present(alertController, animated: true, completion: nil)
    }
    
    
    private class func okAction(_ sender: UIViewController, handler: MMHandlerBlock?) -> UIAlertAction {
        return self.alertAction(MMStrings.ok, style: .`default`, sender: sender, handler: handler)
    }
    
    private class func alertAction(_ title: String, style: UIAlertAction.Style, sender: UIViewController, handler: MMHandlerBlock?) -> UIAlertAction {
        let action = UIAlertAction(title: title, style: style) { (alertAction) in
            handler?()
        }
        
        return action
    }
    
    
}

