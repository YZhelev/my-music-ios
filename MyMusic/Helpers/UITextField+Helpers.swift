//
//  UITextField+Helpers.swift
//  MyMusic
//
//  Created by MacBook Pro on 7.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

extension UITextField {
    
    var safeText: String {
        return self.text ?? ""
    }
    
}
