//
//  String+Localizable.swift
//  MyMusic
//
//  Created by MacBook Pro on 4.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

extension String {
    
    var localized: String {
        let string = self.trimmingCharacters(in: .whitespacesAndNewlines)
        let value  = Bundle.main.localizedString(forKey: string, value: nil, table: nil)
        
        if value != string {
            return value
        }
        
        // Fall back to en
        guard
            let path   = Bundle.main.path(forResource: "en", ofType: "lproj"),
            let bundle = Bundle(path: path) else {
                return value
        }
        
        return NSLocalizedString(string, bundle: bundle, comment: "")
    }
    
}
