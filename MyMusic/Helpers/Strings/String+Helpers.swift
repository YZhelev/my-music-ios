//
//  String+Helpers.swift
//  MyMusic
//
//  Created by MacBook Pro on 7.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

extension String {
    
    var trimmed: String {
        return self.replacingOccurrences(of: " ", with: "")
    }
    
    var withColonSufix: String {
        return self.appending(":")
    }
    
}
