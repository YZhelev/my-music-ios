//
//  NotificationName+Keys.swift
//  MyMusic
//
//  Created by MacBook Pro on 7.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let songsInserted = Notification.Name(rawValue: MMNames.songsInserted)
    
}
