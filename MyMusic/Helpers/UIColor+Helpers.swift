//
//  UIColor+Helpers.swift
//  MyMusic
//
//  Created by MacBook Pro on 4.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

extension UIColor {
    
    var imageFromColor: UIImage? {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(self.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    
    // MARK: Custom Colors
    
    static let mmDetail = UIColor.init(red: 244.0, green: 0.0, blue: 131.0, alpha: 1.0)
    
}
