//
//  Constants.swift
//  MyMusic
//
//  Created by MacBook Pro on 6.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation


struct MMIdentifiers {
    
    struct Cells {
        
        static let list = "listCell"
    
    }
}

struct MMNames {
    
    static let songsInserted = "songsInserted"
    
}
