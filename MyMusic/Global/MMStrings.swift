//
//  MMStrings.swift
//  MyMusic
//
//  Created by MacBook Pro on 4.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

struct MMStrings {
    
    // MARK: Titles
    
    static let addButtonTitle = "TITLE_ADD_MUSIC".localized
    
    
    // MARK: Texts
    
    static let listIsEmpty = "TEXT_MUSIC_LIST_IS_EMPTY".localized
    static let contentType = "TEXT_CONTENT_TYPE".localized
    
    
    // MARK: Alerts
    
    static let myMusic = "ALERT_TITLE_MY_MUSIC".localized
    static let ok      = "ALERT_ACTION_OK".localized
    
    
    // TODO: Add localisation for all strings
    
}
