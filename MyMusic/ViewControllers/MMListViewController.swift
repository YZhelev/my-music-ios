//
//  MMListViewController.swift
//  MyMusic
//
//  Created by MacBook Pro on 6.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class MMListViewController: UIViewController {

    // MARK: Outlets
    
    @IBOutlet weak var listTableView: MMListTableView!
    
    @IBOutlet weak var emptyListView: UIView!
    
    @IBOutlet weak var emptyListLabel: UILabel!
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.listTableView.listDelegate = self
        
        self.emptyListView.alpha = MMDatabaseManager.shared.hasPlaylists ? 0 : 1
        
        self.emptyListLabel.text = MMStrings.listIsEmpty
    }
    
}

extension MMListViewController: MMListTableViewDelegate {
    
    // MARK: List Tableview Delegate
    
    func showListTableView(_ show: Bool) {
        self.emptyListView.alpha = show ? 0 : 1
    }
    
    
    func didSelectSong(_ songInfo: String) {
        UIAlertController.showMessage(songInfo, sender: self)
    }
    
}

