//
//  MMAddViewController.swift
//  MyMusic
//
//  Created by MacBook Pro on 6.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class MMAddViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var contentTextField: UITextField!
    
    @IBOutlet weak var contentTypeLabel: UILabel!
    
    @IBOutlet weak var contentTypeSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var addButton: MMAddButton!
    
    
    // MARK: Constants
    
    private let kSongIndex     : Int = 0
    private let kPlaylistIndex : Int = 1
    
    
    // MARK: Variables
    
    weak var delegate: MMAddViewControllerDelegate?
    
    
    // MARK: Actions
    
    @IBAction func switchContentType(_ sender: Any) {
        self.resignIfHasFirstResponder()
        
        self.checkForm()
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func addContent(_ sender: Any) {
        self.resignIfHasFirstResponder()
        
        let shouldInsertSong = self.contentTypeSegmentedControl.selectedSegmentIndex == kSongIndex
        
        shouldInsertSong ? MMDatabaseManager.shared.insertSong(withName: self.contentTextField.safeText)
                         : MMDatabaseManager.shared.insertPlaylist(MMPlaylist(name: self.contentTextField.safeText))
        
        self.dismiss(animated: true) {
            self.delegate?.didAddContent()
        }
    }
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.contentTextField.delegate = self
        
        self.applyTheme()
        
        self.setData()
    }
    
    
    // MARK: Private Methods
    
    private func checkForm() {
        self.addButton.isEnabled = self.formIsValid
    }
    
    
    // MARK: Helpers
    
    private var formIsValid: Bool {
        if
            self.contentTypeSegmentedControl.selectedSegmentIndex == kPlaylistIndex &&
            self.contentTextField.safeText == MMDatabaseConstants.othersPlaylist {
                UIAlertController.showMessage("The name 'Others' is reserved for playlists.", sender: self)
            
                return false
        }
        
        return !self.contentTextField.safeText.trimmed.isEmpty &&
               self.contentTypeSegmentedControl.selectedSegmentIndex != UISegmentedControl.noSegment
    }
    
    private func applyTheme() {
        let border = UIView(frame: CGRect(x: 0, y: self.contentTextField.frame.height - 1.0, width: self.contentTextField.frame.width, height: 1.0))
        border.backgroundColor = .mmDetail
        
        self.contentTextField.addSubview(border)
        self.contentTextField.tintColor     = .mmDetail
        self.contentTextField.clipsToBounds = true
        
        self.contentTypeSegmentedControl.selectedSegmentIndex = UISegmentedControl.noSegment
        self.contentTypeSegmentedControl.tintColor            = .mmDetail
    }
    
    private func setData() {
        self.contentTextField.placeholder = "Song/Playlist name"
        
        self.contentTypeLabel.text = MMStrings.contentType.withColonSufix
        
        self.contentTypeSegmentedControl.setTitle("Song",     forSegmentAt: kSongIndex)
        self.contentTypeSegmentedControl.setTitle("Playlist", forSegmentAt: kPlaylistIndex)
    }
    
    private func resignIfHasFirstResponder() {
        if self.contentTextField.isFirstResponder {
            self.contentTextField.resignFirstResponder()
        }
    }
    
}

extension MMAddViewController: UITextFieldDelegate {
    
    // MARK: Textfield Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.checkForm()
    }
    
    
}

protocol MMAddViewControllerDelegate: class {
    
    func didAddContent()
    
}
