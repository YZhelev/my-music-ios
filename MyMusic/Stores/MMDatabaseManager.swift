//
//  DatabaseManager.swift
//  MyMusic
//
//  Created by MacBook Pro on 7.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation
import FMDB

final class MMDatabaseManager: NSObject {
    
    
    // MARK: Constants
    
    private let _databaseFileName : String = "myMusic.sqlite"
    
    
    // MARK: Variables
    
    private var _pathToDatabase: String!
    
    private var _dbQueue: FMDatabaseQueue!
    
    
    // MARK: Singleton
    
    static let shared = MMDatabaseManager()
    
    
    // MARK: Initialziers
    
    override init() {
        super.init()
        
        if let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            _pathToDatabase = documentsDirectory.appending("/\(_databaseFileName)")
        }
        
        _dbQueue = FMDatabaseQueue(path: _pathToDatabase)
        
        self.createTables()
    }

    
    // MARK: Insert Methods
    
    private func insertSong(_ song: MMSong) {
        _dbQueue.inTransaction { db, rollback in
            do {
                try db.executeUpdate(song.insertQuery, values: song.values)
                
                DispatchQueue.main.async {
                    NotificationCenter.default.post(Notification(name: .songsInserted))
                }
            } catch {
                rollback.pointee = true
            }
        }
    }
    
    func insertSong(withName name: String) {
        if let othersId = self.othersPlaylistId {
            let song = MMSong(name: name, playlistId: othersId)
            
            self.insertSong(song)
            return
        }
        
        self.insertOthersPlaylistWithSong(named: name)
    }
    
    private func insertOthersPlaylistWithSong(named name: String) {
        let playlist = MMPlaylist(name: MMDatabaseConstants.othersPlaylist)
        
        _dbQueue.inTransaction { db, rollback in
            do {
                try db.executeUpdate(playlist.insertQuery, values: playlist.values)
                
                guard let othersSet = db.executeQuery("SELECT * FROM \(MMDatabaseTables.playlists) WHERE \(MMDatabaseFields.name) = ?", withArgumentsIn: [MMDatabaseConstants.othersPlaylist]) else {
                    return
                }
                
                if othersSet.next() {
                    let playlistId = Int(othersSet.int(forColumn: MMDatabaseFields.id))
                    let song = MMSong(name: name, playlistId: playlistId)
                    
                    try db.executeUpdate(song.insertQuery, values: song.values)
                    
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(Notification(name: .songsInserted))
                    }
                }
            } catch {
                rollback.pointee = true
            }
        }
    }
    
    func insertPlaylist(_ playlist: MMPlaylist) {
        _dbQueue.inTransaction { db, rollback in
            do {
                try db.executeUpdate(playlist.insertQuery, values: playlist.values)
                
                let playlistId = Int(db.lastInsertRowId)
                
                for i in 1...10 {
                    let song = MMSong(name: "Song - \(i)", playlistId: playlistId)
                    
                    try db.executeUpdate(song.insertQuery, values: song.values)
                }
                
                DispatchQueue.main.async {
                    NotificationCenter.default.post(Notification(name: .songsInserted))
                }
            } catch {
                rollback.pointee = true
            }
        }
    }
    
    
    // MARK: Fetch Methods

    var playlists: [MMPlaylist] {
        var lists: [MMPlaylist] = []
        
        _dbQueue.inTransaction { db, rollback in
            guard let listsSet = db.executeQuery("SELECT * FROM \(MMDatabaseTables.playlists)", withArgumentsIn: []) else {
                return
            }
            
            while listsSet.next() {
                lists.append(MMPlaylist(set: listsSet))
            }
        }
        
        return lists
    }
    
    var songs: [MMSong] {
        var songs: [MMSong] = []
        
        _dbQueue.inTransaction { db, rollback in
            guard let songsSet = db.executeQuery("SELECT * FROM \(MMDatabaseTables.songs)", withArgumentsIn: []) else {
                return
            }
            
            while songsSet.next() {
                songs.append(MMSong(set: songsSet))
            }
        }
        
        return songs
    }
    
    
    // MARK: Private Methods
    
    private func createTables() {
        _dbQueue.inTransaction { (db, rollback) in
            db.executeStatements(self.createTablesQuery)
        }
    }
    
    
    // MARK: Public Helpers
    
    var hasPlaylists: Bool {
        var hasList: Bool = false

        _dbQueue.inTransaction { db, rollback in
            if  let listsCount = db.executeQuery("SELECT COUNT(*) AS count FROM \(MMDatabaseTables.playlists)", withArgumentsIn: []),
                listsCount.next() {

                hasList = listsCount.int(forColumn: "count") > 0
            }
        }

        return hasList
    }
    
    
    // MARK: Private Helpers
    
    private lazy var createTablesQuery: String = {
        return """
        \(MMSong.createTableQuery)
        \(MMPlaylist.createTableQuery)
        """
    }()
    
    private var othersPlaylistId: Int? {
        var playlistId: Int?
        
        _dbQueue.inTransaction { db, rollback in
            guard let listsSet = db.executeQuery("SELECT * FROM \(MMDatabaseTables.playlists)", withArgumentsIn: []) else {
                return
            }
            
            while listsSet.next() {
                if listsSet.string(forColumn: MMDatabaseFields.name) == MMDatabaseConstants.othersPlaylist {
                    playlistId = Int(listsSet.int(forColumn: MMDatabaseFields.id))
                }
            }
        }
        
        return playlistId
    }
    
}
