//
//  MMDatabaseConstants.swift
//  MyMusic
//
//  Created by MacBook Pro on 7.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import Foundation

struct MMDatabaseFields {
    
    static let id         = "id"
    static let name       = "name"
    static let playlistId = "playlist_id"
    
}

struct MMDatabaseTables {
    
    static let playlists = "playlists"
    static let songs     = "songs"
    
}

struct MMDatabaseConstants {
    
    static let othersPlaylist = "Others"
    
}
