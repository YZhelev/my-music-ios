//
//  MMListTableViewCell.swift
//  MyMusic
//
//  Created by MacBook Pro on 6.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class MMListTableViewCell: UITableViewCell {
    
    // MARK: Constants
    
    private let kSidePadding: CGFloat = 24.0
    
    
    // MARK: Variables
    
    var songLabel: UILabel!
    
    
    // MARK: Initializers
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.songLabel = UILabel(frame: CGRect(x: self.frame.origin.x + kSidePadding,
                                               y: self.frame.origin.y,
                                               width: self.frame.width - kSidePadding,
                                               height: self.frame.height))
        
        self.songLabel.textAlignment = .left
        self.songLabel.textColor     = .darkGray
        
        self.addSubview(self.songLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
