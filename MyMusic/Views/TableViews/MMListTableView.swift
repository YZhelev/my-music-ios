//
//  MMListTableView.swift
//  MyMusic
//
//  Created by MacBook Pro on 6.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class MMListTableView: UITableView {
    
    // MARK: Constants
    
    private let kRowHeight: CGFloat = 50.0
    
    private let kTagOffset              : Int = 4122
    private let kDefaultExpandedSection : Int = -1
    
    // MARK: Variables
    
    weak var listDelegate: MMListTableViewDelegate?
    
    private var _sections     : [MMPlaylist] = [] {
        didSet{
            self.listDelegate?.showListTableView(!_sections.isEmpty)
        }
    }
    
    private var _sectionsItems : [MMSong]     = []
    
    private var _expandedSection: Int = -1
    
    
    // MARK: Initializers
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.dataSource = self
        self.delegate   = self
        
        self.tableFooterView = UIView()
        
        _sections      = self.playlists
        _sectionsItems = self.songs
        
        
        
        self.register(MMListTableViewCell.classForCoder(), forCellReuseIdentifier: MMIdentifiers.Cells.list)
        
        let notificationName = Notification.Name(rawValue: MMNames.songsInserted)
        NotificationCenter.default.addObserver(self, selector: #selector(self.songsInserted), name: notificationName, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    // MARK: Private Methods
    
    @objc private func songsInserted() {
        _sections      = self.playlists
        _sectionsItems = self.songs
        
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
    
    @objc private func sectionTapped(_ sender: UITapGestureRecognizer) {
        guard let header = sender.view as? MMListHeaderView else {
            return
        }
        
        let section = header.tag - kTagOffset
        
        if (_expandedSection == kDefaultExpandedSection) {
            header.isExpanded = true
            self.expandSection(section)
        } else {
            if (_expandedSection == section) {
                header.isExpanded = false
                self.collapseSection(section)
            } else {
                if let headerToCollapse = viewWithTag(_expandedSection + kTagOffset) as? MMListHeaderView {
                    headerToCollapse.isExpanded = false
                }
                
                self.collapseSection(_expandedSection)
                header.isExpanded = true
                self.expandSection(section)
            }
        }
    }
    
    private func collapseSection(_ section: Int) {
        _expandedSection = kDefaultExpandedSection
        
        self.beginUpdates()
        self.deleteRows(at: indexPathsForSection(section), with: .fade)
        self.endUpdates()
    }
    
    private func expandSection(_ section: Int) {
        _expandedSection = section
        
        self.beginUpdates()
        self.insertRows(at: indexPathsForSection(section), with: .fade)
        self.endUpdates()
    }
    
    private func indexPathsForSection(_ section: Int) -> [IndexPath] {
        var indexPathArray = [IndexPath]()
        
        for i in 0 ..< self.songsForPlaylist(in: section).count {
            let indexPath = IndexPath(row: i, section: section)
            
            indexPathArray.append(indexPath)
        }
        
        return indexPathArray
    }
    
    private func songsForPlaylist(in section: Int) -> [MMSong] {
        let playlist = _sections[section]
        
        return _sectionsItems.filter{$0.playListId == playlist.id}
    }
    
    
    // MARK: Storage Fetch
    
    private var playlists: [MMPlaylist] {
        return MMDatabaseManager.shared.playlists
    }
    
    private var songs: [MMSong] {
        return MMDatabaseManager.shared.songs
    }
    
}

extension MMListTableView: UITableViewDataSource {
    
    // MARK: Tableview Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return _sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if _expandedSection == section {
            return self.songsForPlaylist(in: section).count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MMIdentifiers.Cells.list, for: indexPath)
        
        if let listCell = cell as? MMListTableViewCell {
            listCell.songLabel.text = self.songsForPlaylist(in: indexPath.section)[indexPath.row].name
        }
        
        return cell
    }
    
    
}

extension MMListTableView: UITableViewDelegate {
    
    // MARK: Tableview Delegate
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return kRowHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = MMListHeaderView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.tableView(tableView, heightForHeaderInSection: section)))
        
        headerView.playListLabel.text = _sections[section].name
        headerView.tag = section + kTagOffset
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.sectionTapped(_:)))
        
        headerView.addGestureRecognizer(tapGesture)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.deselectRow(at: indexPath, animated: true)
        
        let songName     = self.songsForPlaylist(in: indexPath.section)[indexPath.row].name
        let playlistName = _sections[indexPath.section].name
        let info         = "Playlist: \(playlistName)\nSong: \(songName)"
        
        self.listDelegate?.didSelectSong(info)
    }
    
}


protocol MMListTableViewDelegate: class {
    
    func didSelectSong(_ songInfo: String)
    
    func showListTableView(_ song: Bool)
    
}
