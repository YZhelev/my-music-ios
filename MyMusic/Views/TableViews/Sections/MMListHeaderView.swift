//
//  MMListHeaderView.swift
//  MyMusic
//
//  Created by MacBook Pro on 6.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class MMListHeaderView: UIView {

    // MARK: Constants
    
    private let kSidePadding     : CGFloat = 16.0
    private let kImageViewSide   : CGFloat = 20.0
    private let kSeparatorHeight : CGFloat = 1.0
    
    private let _animationDuration: TimeInterval = 0.2
    
    
    // MARK: Variables
    
    var playListLabel: UILabel!
    
    var imageView: UIImageView!
    
    var isExpanded: Bool = false {
        didSet {
            self.rotateImage()
        }
    }
    
    
    // MARK: Initializers
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.customInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.customInit()
    }
    
    
    // MARK: Private Methods
    
    private func customInit() {
        self.playListLabel = UILabel(frame: CGRect(x: self.frame.origin.x + kSidePadding,
                                                   y: self.frame.origin.y,
                                                   width: self.frame.width - (kSidePadding * 2) - kImageViewSide,
                                                   height: self.frame.height))
        self.playListLabel.textAlignment = .left
        self.playListLabel.textColor     = .mmDetail
        
        self.imageView = UIImageView(frame: CGRect(x: self.frame.width - kSidePadding - kImageViewSide,
                                                   y: self.frame.origin.y,
                                                   width: kImageViewSide,
                                                   height: kImageViewSide))
        self.imageView.center.y = self.center.y
        self.imageView.image    = #imageLiteral(resourceName: "arrow-down.png")
        
        let separatorView = UIView(frame: CGRect(x: self.frame.origin.x,
                                                 y: self.frame.origin.y,
                                                 width: self.frame.width,
                                                 height: kSeparatorHeight))
        separatorView.backgroundColor = .darkGray
        
        self.addSubview(self.playListLabel)
        self.addSubview(self.imageView)
        self.addSubview(separatorView)
        
        self.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
    }
    
    private func rotateImage() {
        UIView.animate(withDuration: _animationDuration, animations: {
            self.imageView.transform = self.isExpanded ? CGAffineTransform(rotationAngle: CGFloat.pi) : .identity
        })
    }

}
