//
//  MMAddButton.swift
//  MyMusic
//
//  Created by MacBook Pro on 4.04.19.
//  Copyright © 2019 yovkozhelev. All rights reserved.
//

import UIKit

class MMAddButton: UIButton {

    
    // MARK: Initializers
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.customInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.customInit()
    }
    
    
    // MARK: Lifecycle
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let widthEqualsHeight = self.bounds.height == self.bounds.width
        
        self.layer.cornerRadius = widthEqualsHeight ? self.bounds.width / 2 : 16
    }
    
    // MARK: Private Methods
    
    private func customInit() {
        self.setAttributedTitle(self.attributedTitle, for: .normal)
        
        self.setBackgroundImage(UIColor.mmDetail.imageFromColor, for: .normal)
        
        self.tintColor = .white
        self.titleLabel?.numberOfLines = 3
        self.titleLabel?.textAlignment = .center
        
        self.clipsToBounds = true
    }
    
    
    // MARK: Helpers
    
    private var attributedTitle: NSAttributedString {
        let attrTitle = NSMutableAttributedString(string: MMStrings.addButtonTitle)
        
        attrTitle.addAttributes([.font : UIFont.systemFont(ofSize: self.frame.height / 2.8, weight: .bold)], range: NSRange(location: 0, length: 1))
        attrTitle.addAttributes([.font : UIFont.systemFont(ofSize: self.frame.height / 8, weight: .medium)], range: NSRange(location: 1, length: attrTitle.string.dropFirst().count))
        
        return attrTitle
    }
    
}
